package structures

import java.util.*

public open class TreeNode: Cloneable<TreeNode> {
    override fun clone(): TreeNode {
        return this;
    }

    protected var children: ArrayList<TreeNode> = ArrayList<TreeNode>();
    private var parent: TreeNode? = null;
    public var height = 1;

    public constructor() {}

    public constructor(parent: TreeNode) {
        this.parent = parent;
        this.height= parent.height + 1
    }

    public fun getChildren(): List<TreeNode> {
        return children;
    }

    public fun setParent(parent: TreeNode?) {
        this.parent = parent;
        if (parent != null) {
            this.height = parent.height + 1
        }
    }

    public fun getParent(): TreeNode? {
        return this.parent;
    }

    public fun addChild(child: TreeNode) {
        child.setParent(this);
        this.children.add(child);
        child.height = this.height + 1
    }

    public fun isRoot() : Boolean {
        return (this.parent == null);
    }

    public fun isLeaf(): Boolean {
        return this.children.size == 0;
    }

    public fun removeParent() {
        this.parent = null;
    }

    public fun getDepth(): Int {
        return bfs().height
    }

    public fun getPath(node: TreeNode?): LinkedList<TreeNode> {
        return searchUp({n -> n.parent!!.equals(node)});
    }

    public fun getLongestPath(): LinkedList<TreeNode> {
        return bfs().searchUp()
    }

    public fun bfs(blockAction: ((Int, TreeNode) -> Unit)? = null, levelAction: ((Int) -> Unit)? = null): TreeNode {
        val depths: HashMap<TreeNode, Int> = HashMap();
        var q = LinkedList<TreeNode>()
        var nodes = 0;
        q.push(this);
        depths.put(this, 0);
        var maxHeight = 0;
        while (q.isNotEmpty()) {
            val node = q.pop();
            if (blockAction != null) {
                blockAction(depths.get(node)!!, node);
            }
            nodes++;
            node.children.forEach { child ->
                if (!depths.contains(child)) {
                    q.push(child)
                    val currentHeight = depths.get(node)!!+1;
                    if (currentHeight > maxHeight) {
                        maxHeight = currentHeight;
                        if (levelAction != null) {
                            levelAction(maxHeight)
                        }

                    }
                    depths.put(child, currentHeight)
                }
            }
        }
        val (treeNode, index) = depths.entries.maxBy { entry: Map.Entry<TreeNode, Int> -> entry.value }!!
        return treeNode
    }
    public fun searchUp(stopCondition: ((TreeNode) -> Boolean)? = null, limit:Int=5000): LinkedList<TreeNode> {
        var q = LinkedList<TreeNode>()
        var nodes = 1;
        var node = this;
        q.add(node)
        while (node.parent != null && nodes <= limit) {
            nodes++;
            q.addFirst(node.parent)
            if (stopCondition != null && stopCondition(node)) {
                break
            }
            node = node.parent!!;
        }
        return q;
    }

    public fun prune(limit: Int = 5000): LinkedList<TreeNode> {
        var nodes = 1;
        var node = this;
        val removedNodes = LinkedList<TreeNode>()
        while (node.parent != null && nodes <= limit) {
            nodes ++
            val parentChildren: List<TreeNode> = node.getParent()!!.children.clone() as List<TreeNode>
            parentChildren.forEachIndexed { i, child ->
                if (child != node) {
                    child.setParent(null)
                    removedNodes.add(child)
                    node.getParent()!!.children.remove(child)
                }
            }
            node = node.getParent()!!
        }
        return removedNodes
    }

    fun forEach(blockAction: (Int, TreeNode) -> Unit, levelAction: (Int) -> Unit) {
        bfs(blockAction, levelAction)
    }
}