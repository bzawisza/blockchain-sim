package structures

interface Cloneable<T> {
    public fun clone(): T;
}