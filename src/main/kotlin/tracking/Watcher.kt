package tracking

import blockchain.Block
import blockchain.BlockChain
import blockchain.Node
import blockchain.Transaction
import events.EventBus.subscribe
import events.EventType.*
import mu.KotlinLogging
import structures.TreeNode
import java.util.*

object Watcher {
    private val N = 6
    private val logger = KotlinLogging.logger {}
    private val nodes: HashMap<String, Node> = HashMap()
    private var connectCount: Int = 0
    private var running = true

    private val connections: HashMap<String, HashMap<String, Long>> = HashMap()
    private val disconnections: HashMap<String, HashMap<String, ArrayList<Pair<Long, Long>>>> = HashMap()
    private val consensi: HashMap<String, ArrayList<Triple<String, Int, Int>>> = HashMap()

    private val transactions: HashMap<UUID, Transaction> = HashMap()
    private val receivedTransactions: HashMap<String, ArrayList<Transaction>> = HashMap()
    private val syncedTransactions: HashMap<String, ArrayList<Transaction>> = HashMap()
    private val reintroducedTransactions: HashMap<UUID, Int> = HashMap()

    private val blocks: HashMap<UUID, Block> = HashMap()
    private val receivedBlocks: HashMap<String, ArrayList<Block>> = HashMap()
    private val droppedBlocks: HashMap<String, Int> = HashMap()

    private val blocksInStorage: HashMap<String, HashMap<UUID, Pair<Long, Block>>> = HashMap()
    private val blocksRemovedFromStorage: HashMap<String, HashMap<UUID, ArrayList<Triple<Long, Long, Block>>>> = HashMap()
    private val blocksAddedFromStorage: HashMap<String, HashMap<UUID, Triple<Long, Long, Block>>> = HashMap()

    private val time: Long

    init {
        this.time = System.currentTimeMillis()
        initSubscriptions()
    }

    fun reset() {
        nodes.clear()
        connectCount = 0
        running = true
        connections.clear()
        disconnections.clear()
        consensi.clear()
        transactions.clear()
        receivedTransactions.clear()
        syncedTransactions.clear()
        reintroducedTransactions.clear()
        blocks.clear()
        receivedBlocks.clear()
        droppedBlocks.clear()
        blocksInStorage.clear()
        blocksRemovedFromStorage.clear()
        blocksAddedFromStorage.clear()
    }

    private fun initSubscriptions() {
        subscribe(NEW_NODE, { event -> onCreate(event.sender, event.data as Node) })

        subscribe(CONNECT, { event -> onConnect(event.sender, event.data as String) })
        subscribe(DISCONNECT, { event -> onDisconnect(event.sender, event.data as String) })
        subscribe(CONSENSUS, { event -> onConsensus(event.sender, event.data as Triple<String, Int, Int>) })
        
        subscribe(NEW_TRANSACTION, { event -> onNewTransaction(event.sender, event.data as Transaction) })
        subscribe(RECEIVE_TRANSACTION, { event -> onReceiveTransaction(event.sender, event.data as Transaction) })
        subscribe(CONNECT_NEW_TRANSACTION, { event -> onConnectAndNewTransaction(event.sender, event.data as Transaction) })

        subscribe(MINE_BLOCK, { event -> onMineBlock(event.sender, event.data as Block) })
        subscribe(RECEIVE_BLOCK, { event -> onReceiveBlock(event.sender, event.data as Block) })

        subscribe(ADD_TO_STORAGE, { event -> onAddToStorage(event.sender, event.data as Block) })
        subscribe(CLEAR_STORAGE, { event -> onClearStorage(event.sender, event.data as List<Block>) })
        subscribe(ADD_FROM_STORAGE, { event -> onAddFromStorage(event.sender, event.data as Block) })

        subscribe(DROP_BLOCKS, { event -> onDropBlocks(event.sender, event.data as List<Block>) })
        subscribe(REINTRODUCED_TRANSACTION, { event -> onReintroduceTransactions(event.sender, event.data as Transaction) })
    }

    private fun onCreate(sender: String, node: Node) {
        nodes.put(sender, node);
    }

    private fun onReintroduceTransactions(sender: String, transaction: Transaction) {
        logger.trace { "Node ${sender.takeLast(N)} has reintroduced transaction ${transaction.uuid.toString().takeLast(N)}"}
        reintroducedTransactions.putIfAbsent(transaction.uuid, 0)
        reintroducedTransactions.put(transaction.uuid, reintroducedTransactions.get(transaction.uuid)!!+1)
    }

    private fun onDropBlocks(sender: String, blocks: List<Block>) {
        logger.info { "Node ${sender.takeLast(N)} has dropped blocks ${blocks.map{block->block.uuid.toString().takeLast(N)}}"}
        droppedBlocks.putIfAbsent(sender, 0)
        droppedBlocks.put(sender, droppedBlocks.get(sender)!!+blocks.size)
    }

    private fun onAddFromStorage(sender: String, block: Block) {
        logger.info { "Node ${sender.takeLast(N)} has added block ${block.uuid.toString().takeLast(N)} from storage"}
        val removedBlockPair = blocksInStorage.get(sender)!!.remove(block.uuid)!!
        blocksAddedFromStorage.putIfAbsent(sender, hashMapOf())
        blocksAddedFromStorage.get(sender)!!.put(block.uuid, Triple(removedBlockPair.first, System.currentTimeMillis(), block))
    }

    private fun onClearStorage(sender: String, blocks: List<Block>) {
        logger.info { "Node ${sender.takeLast(N)} has cleared out the following blocks in storage: ${blocks.map { block -> block.uuid.toString().takeLast(N) }.reduce{a,b -> a + ", " + b }} "}
        blocksRemovedFromStorage.putIfAbsent(sender, hashMapOf())
        blocks.forEach {block ->
            blocksRemovedFromStorage.get(sender)!!.putIfAbsent(block.uuid, arrayListOf())
            val removedBlockPair = blocksInStorage.get(sender)!!.remove(block.uuid)!!
            blocksRemovedFromStorage.get(sender)!!.get(block.uuid)!!.add(Triple(removedBlockPair.first, System.currentTimeMillis(), block))
        }

    }

    private fun onAddToStorage(sender: String, block: Block) {
        logger.info { "Node ${sender.takeLast(N)} added block ${block.uuid.toString().takeLast(N)} to storage" }
        blocksInStorage.putIfAbsent(sender, hashMapOf())
        blocksInStorage.get(sender)!!.put(block.uuid, Pair(System.currentTimeMillis(), block))
    }

    private fun onReceiveBlock(sender: String, block: Block) {
        logger.info { "Node ${sender.takeLast(N)} received block ${block.uuid.toString().takeLast(N)} "}
        receivedBlocks.putIfAbsent(sender, arrayListOf())
        receivedBlocks.get(sender)!!.add(block)
    }

    private fun onMineBlock(sender: String, block: Block) {
        logger.info { "Node ${sender.takeLast(N)} mined a new block ${block.uuid.toString().takeLast(N)} . Hash: [${block.hash.takeLast(N)}]"}
        blocks.put(block.uuid, block)
    }

    private fun onConnectAndNewTransaction(sender: String, transaction: Transaction) {
//        logger.info { "Node ${sender.takeLast(N)} received transaction ${transaction.uuid.toString().takeLast(N)} upon initially connecting and syncing"}
        syncedTransactions.putIfAbsent(sender, arrayListOf())
        syncedTransactions.get(sender)!!.add(transaction)
    }

    private fun onReceiveTransaction(sender: String, transaction: Transaction) {
        logger.trace { "Node ${sender.takeLast(N)} received transaction ${transaction.uuid.toString().takeLast(N)} "}
        receivedTransactions.putIfAbsent(sender, arrayListOf())
        receivedTransactions.get(sender)!!.add(transaction)
    }

    private fun onNewTransaction(sender: String, transaction: Transaction) {
        logger.trace { "Node ${sender.takeLast(N)} created new transaction ${transaction.uuid.toString().takeLast(N)}" }
        transactions.put(transaction.uuid, transaction)
    }

    private fun onConsensus(sender: String, triple: Triple<String, Int, Int>) {
        if (sender.equals(triple.first)) {
            logger.info { "Node ${sender.takeLast(N)} ran consensus. It has the longest chain. Length: ${triple.third}" }
        } else {
            logger.info { "Node ${sender.takeLast(N)} ran consensus. New chain received by ${triple.first.takeLast(N)}. Length: ${triple.second}->${triple.third}" }
        }
        consensi.putIfAbsent(sender, arrayListOf())
        consensi.get(sender)!!.add(triple)
    }

    private fun onDisconnect(sender: String, disconnected: String) {
        logger.info { "Node ${sender.takeLast(N)} disconnected from ${disconnected.takeLast(N)} " }
        val now = System.currentTimeMillis()
        val connectedTime = connections.get(sender)!!.remove(disconnected)!!
        disconnections.putIfAbsent(sender, hashMapOf())
        disconnections.get(sender)!!.putIfAbsent(disconnected, arrayListOf())
        disconnections.get(sender)!!.get(disconnected)!!.add(Pair(connectedTime, now))
    }

    private fun onConnect(sender: String, connected: String) {
        logger.info { "Node ${sender.takeLast(N)} connected to ${connected.takeLast(N)} " }
        connections.putIfAbsent(sender, hashMapOf())
        connections.get(sender)!!.put(connected, System.currentTimeMillis())
    }

    fun getNodes(): HashMap<String, Node> {
        return nodes
    }

    fun getNodeCount(): Int {
        return nodes.size
    }

    fun getBlockchains(): List<BlockChain> {
        return nodes.values.map { node -> node.getBlockchain() }
    }

    fun getBlockchainSizes(): List<Int> {
        return nodes.values.map { nodes -> nodes.getBlockchain().getLongestHeight() }
    }

    fun getLongestBlockchain(): LinkedList<TreeNode> {
        return nodes.values.maxBy { node -> node.getBlockchain().getLongestHeight() }!!.getBlockchain().getBlocksInLongestChain()
    }

    fun getLongestBlockchainSize(): Int {
        return getBlockchainSizes().max()!!
    }

    fun isBlockInLongestChain(blockUUID: UUID): Boolean {
        return getLongestBlockchain().map { block -> (block as Block).uuid }.contains(blockUUID)
    }

    fun isTransactionInLongestChain(transactionUUID: UUID): Boolean {
        return getLongestBlockchain().flatMap { block -> (block as Block).data.map { transaction -> transaction.uuid } }.contains(transactionUUID)
    }

    fun percentageNodesHasTransaction(transactionUUID: UUID): Double {
        val tuids = getBlockchains().map { blockChain -> blockChain.getBlocksInLongestChain().map { block -> (block as Block).data.map { transaction -> transaction.uuid }.contains(transactionUUID) }.filter { true }.isNotEmpty() }
        if (tuids.size > 0) {
            return (tuids.filter { tuid -> tuid }.size.toDouble()) / tuids.size;
        } else {
            return 0.0
        }
    }

    fun percentageNodesHasBlock(blockUUID: UUID): Double {
        val buids = getBlockchains().map { blockchain -> blockchain.getBlocksInLongestChain().map {block -> (block as Block).uuid == blockUUID }.filter { true }.isNotEmpty() }
        if (buids.size > 0) {
            return (buids.filter { buid -> buid }.size.toDouble()) / (buids.size);
        } else {
            return 0.0
        }
    }

    fun numberOfEqualBlockchainSizes(): Int {
        return HashSet(getBlockchainSizes()).size
    }

    fun getOpenConnections(): HashMap<String, HashMap<String, Long>> {
       return connections
    }

    fun getClosedConnections(): HashMap<String, HashMap<String, ArrayList<Pair<Long, Long>>>> {
        return disconnections
    }

    fun getConsensi(): HashMap<String, ArrayList<Triple<String, Int, Int>>> {
        return consensi
    }

    fun getTransactions(): HashMap<UUID, Transaction> {
       return transactions
    }

    fun getReceivedTransactions(): HashMap<String, ArrayList<Transaction>> {
        return receivedTransactions
    }

    fun getSyncedTransactions(): HashMap<String, ArrayList<Transaction>> {
        return syncedTransactions
    }

    fun getBlocks(): HashMap<UUID, Block> {
        return blocks
    }

    fun getReceivedBlocks(): HashMap<String, ArrayList<Block>> {
        return receivedBlocks
    }

    fun getDroppedBlocks(): HashMap<String, Int> {
        return droppedBlocks
    }

    fun getReintroducedTransactions(): HashMap<UUID, Int> {
        return reintroducedTransactions
    }

    fun getBlocksInStorage(): HashMap<String, HashMap<UUID, Pair<Long, Block>>> {
        return blocksInStorage;
    }

    fun getBlocksRemovedFromStorage(): HashMap<String, HashMap<UUID, ArrayList<Triple<Long, Long, Block>>>> {
        return blocksRemovedFromStorage
    }

    fun getBlocksAddedFromStorage(): HashMap<String, HashMap<UUID, Triple<Long, Long, Block>>> {
        return blocksAddedFromStorage
    }

    fun getTimePassed(): Long {
        return System.currentTimeMillis() - this.time
    }

}
