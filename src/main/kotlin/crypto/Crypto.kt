package crypto

import java.io.Serializable
import java.security.*
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec

class Crypto {
    var keyGen = KeyPairGenerator.getInstance("EC")
    var random = SecureRandom.getInstance("SHA1PRNG")
    val dsaSign = Signature.getInstance("SHA1withECDSA")
    val dsaVerify = Signature.getInstance("SHA1withECDSA")

    fun gen(): KeyPair? {
        keyGen.initialize(256, random);
        return keyGen.generateKeyPair();
    }

    fun keyToHexString(key: Key): String {
        val sb = StringBuilder()
        key.encoded.forEach { byte: Byte -> sb.append(String.format("%02x", byte)) }
        return sb.toString();
    }

    fun byteToPrivateKey(byteArray: ByteArray): PrivateKey? {
        return KeyFactory.getInstance("EC").generatePrivate(PKCS8EncodedKeySpec(byteArray))
    }

    fun byteToPublicKey(byteArray: ByteArray): PublicKey? {
        return KeyFactory.getInstance("EC").generatePublic(X509EncodedKeySpec(byteArray))
    }

    fun sign(txt: String, pk: PrivateKey): ByteArray? {
        dsaSign.initSign(pk)
        dsaSign.update(txt.toByteArray())
        return dsaSign.sign()
    }

    fun sign(msg: String, privateString: String): ByteArray? {
        val pk: PrivateKey = byteToPrivateKey(hexStringToByteArray(privateString))!!
        val sign = sign(msg, pk)
        return sign;
    }

    fun verify(txt: String, publicKey: PublicKey, hash: ByteArray): Boolean {
        dsaVerify.initVerify(publicKey);
        dsaVerify.update(txt.toByteArray())
        return dsaVerify.verify(hash)
    }

    fun verify(message: String, publicString: String, hash: ByteArray): Boolean {
        val publicKey = byteToPublicKey(hexStringToByteArray(publicString))!!
        return verify(message, publicKey, hash);
    }

    fun hexStringToByteArray(s: String): ByteArray {
        val len = s.length
        val data = ByteArray(len / 2)
        var i = 0
        while (i < len) {
            data[i / 2] = ((Character.digit(s[i], 16) shl 4) + Character.digit(s[i + 1], 16)).toByte()
            i += 2
        }
        return data
    }
}