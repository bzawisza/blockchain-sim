package crypto

import java.security.MessageDigest
import javax.xml.bind.DatatypeConverter

class Hash {
    private val digest = MessageDigest.getInstance("SHA-256")!!
    fun doHash(content: String): String {
        val content = "$content"
        digest.update(content.toByteArray(Charsets.UTF_8))
        val hashBytes = digest.digest()
        return DatatypeConverter.printHexBinary(hashBytes)
    }
}