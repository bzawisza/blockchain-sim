package blockchain

import structures.TreeNode
import java.util.*

class ConsensusHelper(val syncTime: Int = 60000) {
    private var lastConsensus: Long = 0L

    fun isTimeForConsensus(): Boolean {
        return System.currentTimeMillis() - lastConsensus > syncTime
    }

    fun ranConsensus() {
        lastConsensus = System.currentTimeMillis()
    }

    fun getSortedSizesForPeers(peers: Map<String, Node>, dropThreshold:Int): List<Pair<String, Int>> {
        return peers.entries
                .map { entry -> Pair(entry.key, entry.value.getBlockchain().getLongestHeight()) }
                .sortedByDescending { addressSizePair -> addressSizePair.second }
                .dropWhile { addressSizePair -> dropThreshold >= addressSizePair.second }
    }

    fun getNewBlocks(thisBlockChain: BlockChain, otherBlockChain: BlockChain): Pair<Block?, List<Block>> {
        var node = otherBlockChain.getLastBlockInLongestChain()
        val searchUp: LinkedList<TreeNode> = node.searchUp(limit = 5000)
        val newBlocks: ArrayList<Block> = arrayListOf()
        var parent: Block? = null;
        for (block in searchUp.descendingIterator()) {
            val foundBlock: Block? = thisBlockChain.findBlockForHash((block as Block).hash)
            if (foundBlock != null) {
                parent = foundBlock
                break;
            }
            newBlocks.add(block.clone())
        }
        return Pair(parent, newBlocks);
    }

    fun getLongestValidChain(sortedSizesForPeers: List<Pair<String, Int>>, peers: Map<String, Node>, blockchain: BlockChain): Triple<String?, Block?, List<Block>> {
        var longestSize = 0
        var longestChain = emptyList<Block>()
        var longestChainParent: Block? = null
        var longestPeer: String? = null
        for ((peerAddress, peerSize) in sortedSizesForPeers) {
            if (longestSize > peerSize) break;
            val peer = peers.get(peerAddress)
            if (peer == null) continue;
            val(parent: Block?, newBlocks: List<Block>) = getNewBlocks(blockchain, peer.getBlockchain())
            val verifiedBlocks: List<Block> = filterUnverified(newBlocks)
            val verifiedBlocksSize = verifiedBlocks.size
            if (parent == null || verifiedBlocks.isEmpty() || verifiedBlocksSize <= longestSize) continue;
            longestSize = verifiedBlocksSize
            longestChain = verifiedBlocks
            longestChainParent = parent
            longestPeer = peerAddress
        }
        return Triple(longestPeer, longestChainParent, longestChain)
    }

    private fun filterUnverified(blocks: List<Block>): List<Block> {
        val verifiedBlocks: LinkedList<Block> = LinkedList()
        val previous: Block? = null;
        for (block in blocks) {
            if (previous != null) {
                block.previousHash = previous.hash
            }
            block.updateHash()
            if (block.verify()) {
                verifiedBlocks.addFirst(block)
            }
        }
        return verifiedBlocks
    }
}