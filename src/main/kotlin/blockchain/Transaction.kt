package blockchain

import crypto.Crypto
import structures.Cloneable
import java.util.*

class Transaction: Cloneable<Transaction> {
    private lateinit var from: String
    private lateinit var to: String
    var value: Any = 0
    var timestamp: Long = 0;
    lateinit var uuid: UUID
    private var signature: ByteArray? = null
    private var message: String? = null

    constructor(from: String, to: String, amount: Any) {
        this.timestamp = System.currentTimeMillis()
        this.from = from
        this.to = to
        this.value = amount
        this.uuid = UUID.randomUUID()
    }

    private constructor()

    override fun clone(): Transaction {
        val other = Transaction()
        other.from = from;
        other.to = to;
        other.value = value;
        other.timestamp = timestamp
        other.uuid = uuid;
        other.signature = signature
        other.message = message
        return other
    }

    fun sign(key: String) {
        this.message = "$timestamp$uuid$from$to$value"
        this.signature = Crypto().sign(this.message!!, key)
    }

    override fun toString(): String {
        return "Transaction(from='$from', to='$to', value=$value, uuid=$uuid, signature=${Arrays.toString(signature)}, message='$message')"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Transaction) {
            val signed: Boolean? = signature?.equals(other.signature)
            return uuid.equals(other.uuid) && message.equals(other.message) && (signed == true)
        } else {
            return this.equals(other)
        }
    }

    fun verify(): Boolean {
        return Crypto().verify(this.message!!, this.from, this.signature!!)
    }

}
