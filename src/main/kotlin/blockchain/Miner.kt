package blockchain

import events.EventBus
import events.Event
import events.EventType
import mu.KLogging
import java.util.*


open class Miner: Node {
    companion object: KLogging()
    var sleepRange:Int
    protected var difficultyPrefix: String
    protected var transactions_per_block: Int

    constructor(): this(0, 0, 100)
    constructor(difficulty: Int, sleepRange: Int): this(difficulty, sleepRange,100,60000)
    constructor(difficulty: Int, sleepRange: Int, consensusTime: Int): this(difficulty, sleepRange, 100, consensusTime)
    constructor(difficulty: Int, sleepRange: Int, transactions_per_block: Int, consensusTime: Int): this(difficulty, sleepRange, emptyList<Node>(), transactions_per_block, consensusTime)

    constructor(difficulty: Int, sleepRange: Int, connectTo: List<Node>, transactions_per_block: Int, consensusTime: Int) : super("miner", connectTo = connectTo, CONSENSUS_TIME = consensusTime) {
        this.sleepRange = sleepRange
        this.difficultyPrefix = "0".repeat(difficulty)
        this.transactions_per_block = transactions_per_block
    }

    val random = Random()

    private fun mine(block: Block): Boolean {
        val currentTimeMillis = System.currentTimeMillis()
        print("Mining block... ")
        while (!block.verify(verifyTransactions = false)) { // don't need to verify transactions since they are verified on being received
            block.nonce = random.nextLong();
            block.updateHash()
            if (consensus()) {
                return false;
            }
            Thread.yield()
            Thread.sleep((Math.random()*sleepRange).toLong())
        }
        logger.info { " Done. Time was ${System.currentTimeMillis() - currentTimeMillis} nonce is ${block.nonce}, hash is ${block.hash}. UUID ${block.uuid}" }
        return true;
    }

    open fun cleanupTransactions() {}

    fun mineblocks(condition: (Int) -> Boolean) {
        var count = 0;
        while (!condition(count)) {
            if (getMempool().getPendingTransactions().size > 0) {
                cleanupTransactions()
                var pendingTransactions: List<Transaction> = getMempool().getPendingTransactions().take(transactions_per_block);
                val last_block = getBlockchain().getLastBlockInLongestChain().clone();
                // add some kind of reward here? TODO
                // TODO: check and subtract $$$
                val block = Block(System.currentTimeMillis(), pendingTransactions, last_block.hash, difficultyPrefix)
                val result = mine(block);
                if (result) {
                    count++;
                    getBlockchain().add(block, verified = true)
                    getMempool().completeTransactions(pendingTransactions)
                    EventBus.dispatch(Event(EventType.MINE_BLOCK, this.publicKey, block))
                    getPeers().forEach { addr: String, node: Node -> node.receiveBlock(block.clone()) }
                }
                Thread.yield()
                Thread.sleep(sleepRange*1000L)
            }
        }
    }

    override fun receiveBlock(block: Block) {
        if (getBlockchain().add(block)) {

            EventBus.dispatch(Event(EventType.RECEIVE_BLOCK, this.publicKey, block))
            getPeers().forEach { addr: String, node: Node -> node.receiveBlock(block.clone()) }
        }
    }

    override fun receiveTransaction(transaction: Transaction) {
        val verified = transaction.verify();
        if (verified) {
            if (getMempool().addIfNotPresent(transaction)) {
                EventBus.dispatch(Event(EventType.RECEIVE_TRANSACTION, this.publicKey, transaction))
                propagateTransaction(transaction)
            }
        }
    }

    private fun propagateTransaction(transaction: Transaction) {
        getPeers().values.forEach { peer -> peer.receiveTransaction(transaction.clone()) }
    }

}