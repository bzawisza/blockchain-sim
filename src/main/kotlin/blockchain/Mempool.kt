package blockchain

import events.EventBus
import events.Event
import events.EventType
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class Mempool(maxSize: Int, private val pool: ConcurrentHashMap<UUID, Transaction> = ConcurrentHashMap(maxSize), private val finished: ConcurrentHashMap<UUID, Transaction> = ConcurrentHashMap(maxSize), private val owner: String) {

    val lock: Lock = ReentrantLock();

    fun getPendingTransactions(): MutableCollection<Transaction> {
        return pool.values
    }

    fun completeTransactions(transactions: List<Transaction>) {
        lock.withLock {
            transactions.forEach { transaction: Transaction ->
                finished.putIfAbsent(transaction.uuid, transaction)
                pool.remove(transaction.uuid)
            }
        }
    }

    fun completeTransaction(transaction: Transaction) {
        lock.withLock {
            finished.putIfAbsent(transaction.uuid, transaction)
            pool.remove(transaction.uuid)
        }
    }

    fun deleteTransaction(transaction: Transaction) {
        lock.withLock {
//            finished.remove(transaction.uuid)
            pool.remove(transaction.uuid)
        }
    }

    fun addIfNotPresent(transaction: Transaction): Boolean {
        lock.withLock {

            if (!finished.contains(transaction.uuid) && !pool.containsKey(transaction.uuid)) {
                pool.put(transaction.uuid, transaction)
                return true
            }
            return false
        }
    }

    fun addUnfininishedTransactions(transactions: MutableCollection<Transaction>) {
        lock.withLock {
            transactions.forEach { transaction ->
                if (!finished.contains(transaction.uuid))
                    if (pool.putIfAbsent(transaction.uuid, transaction) == null) {
                        EventBus.dispatch(Event(EventType.CONNECT_NEW_TRANSACTION, owner, transaction))
                    }
            }
        }
    }

    fun resync(removedTransactions: List<Transaction>, transactionsToComplete: List<Transaction>): LinkedList<Transaction> {
        val reintroduced: HashSet<Transaction> = HashSet()
        val newTransactions: LinkedList<Transaction> = LinkedList()
        lock.withLock {
            removedTransactions.forEach { transaction ->
                if (finished.remove(transaction.uuid) != null) {
                    newTransactions.add(transaction)
                }
                if (pool.putIfAbsent(transaction.uuid, transaction) == null) {
                    reintroduced.add(transaction)
                }
            }
            transactionsToComplete.forEach { transaction ->
                finished.putIfAbsent(transaction.uuid, transaction)
                if (pool.remove(transaction.uuid) != null) {
                    reintroduced.remove(transaction)
                }
            }
        }
        reintroduced.forEach{transaction ->
            EventBus.dispatch(Event(EventType.REINTRODUCED_TRANSACTION, owner, transaction))
        }
        return newTransactions
    }
}