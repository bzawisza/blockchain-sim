package blockchain

import crypto.Crypto
import events.EventBus
import events.Event
import events.EventType
import java.security.KeyPair
import java.util.concurrent.ConcurrentHashMap

abstract class Node(val type: String, pk: String? = null, key: String? = null, connectTo: List<Node> = emptyList(), var CONSENSUS_TIME: Int = 60000) {

    private val consensusHelper = ConsensusHelper(CONSENSUS_TIME)

    private val mempool: Mempool
    private val peers: ConcurrentHashMap<String, Node> = ConcurrentHashMap()
    private val blockchain: BlockChain

    lateinit var publicKey: String
    lateinit var privateKey: String


    init {
        if (pk == null) { newKeys() }
        EventBus.dispatch(Event(EventType.NEW_NODE, this.publicKey, this))
        mempool = Mempool(10000,owner=this.publicKey)
        blockchain = BlockChain(this.publicKey)
        connectTo.forEach{node -> connect(node)}
    }

    fun getPeers(): ConcurrentHashMap<String, Node> {
        return peers
    }

    private fun generate_ECDSA_keys(): KeyPair? {
        return Crypto().gen()
    }

    fun newKeys(): Array<String> {
        val keyPair = generate_ECDSA_keys()
        publicKey = Crypto().keyToHexString(keyPair!!.public)
        privateKey = Crypto().keyToHexString(keyPair.private)
        return arrayOf(publicKey, privateKey)
    }

    fun getMempool(): Mempool {
        return mempool
    }

    fun getBlockchain(): BlockChain {
        return blockchain;
    }

    fun consensus(force: Boolean = false, peers: Map<String, Node> = this.peers): Boolean {
        if (consensusHelper.isTimeForConsensus() || force) {
            val lastBCLength = blockchain.getLongestHeight()
            if (this.peers == peers) consensusHelper.ranConsensus()
            val sortedSizesForPeers: List<Pair<String, Int>> = consensusHelper.getSortedSizesForPeers(peers, blockchain.getLongestHeight())
            val (peerAddress, parent, verifiedBlocks) = consensusHelper.getLongestValidChain(sortedSizesForPeers, peers, blockchain)
            val foundLongerBc = !(peerAddress == null || parent == null || verifiedBlocks.isEmpty())
            if (foundLongerBc) {
                blockchain.addAll(parent, verifiedBlocks)
            }

            val removedBlocks = blockchain.prune()
            EventBus.dispatch(Event(EventType.DROP_BLOCKS, this.publicKey, removedBlocks))
            val removedTransactions = removedBlocks.flatMap{ node -> (node as Block).data }
            val transactionsToComplete = verifiedBlocks.flatMap { node -> node.data }
            val newTransactions = mempool.resync(removedTransactions, transactionsToComplete)
            if (peerAddress != null) {
                newTransactions.forEach { transaction -> getPeers().get(peerAddress)?.receiveTransaction(transaction.clone()) }
            }
            blockchain.clearStorage()
            val newBCLength = blockchain.getLongestHeight()
            if (foundLongerBc) {
                EventBus.dispatch(Event(EventType.CONSENSUS, this.publicKey, Triple(peerAddress, lastBCLength, newBCLength)))
            } else {
                EventBus.dispatch(Event(EventType.CONSENSUS, this.publicKey, Triple(this.publicKey, lastBCLength, newBCLength)))
            }

            return true
        }
        return false;
    }

    fun connect(node: Node, connectBack: Boolean = false) {
        if (!getPeers().containsKey(node.publicKey)) {
            EventBus.dispatch(Event(EventType.CONNECT, this.publicKey, node.publicKey))
        }
        if (!connectBack) {
            node.connect(this, connectBack = true)
        }
        getPeers().put(node.publicKey, node)
        mempool.addUnfininishedTransactions(node.getMempool().getPendingTransactions())
        consensus(force=true, peers= hashMapOf(Pair(node.publicKey, node)))
    }

    fun disconnect(node: Node, disconnectBack: Boolean = false) {
        if (getPeers().containsKey(node.publicKey)) {
            EventBus.dispatch(Event(EventType.DISCONNECT, this.publicKey, node.publicKey))
        }
        if (!disconnectBack) {
            getPeers().get(node.publicKey)!!.disconnect(this, disconnectBack = true)
        }
        getPeers().remove(node.publicKey)
    }


    fun create_transaction(to: String, data: Any): Transaction? {
        if (to.length == 182) {
            val transaction: Transaction = Transaction(publicKey, to, data)
            transaction.sign(privateKey)
            EventBus.dispatch(Event(EventType.NEW_TRANSACTION, this.publicKey, transaction))
            this.receiveTransaction(transaction)
//            getPeers().values.forEach { peer -> peer.receiveTransaction(transaction.clone()) }
            return transaction
        } else {
            println("Wrong address. Length: ${to.length}")
            return null
        }
    }

    abstract fun receiveBlock(block: Block)
    abstract fun receiveTransaction(transaction: Transaction);

}