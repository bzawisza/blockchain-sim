package blockchain

import events.EventBus
import events.Event
import events.EventType
import structures.TreeNode
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.locks.Lock
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class BlockChain {

    val treeLock: Lock;
    private val root: Block;
    private val leaves: ConcurrentHashMap<String, Block>;
    private val hashToBlock: HashMap<String, Block>;
    var storage: ConcurrentHashMap<String, Block>;
    val owner: String

    constructor(owner: String) {
        treeLock = ReentrantLock()
        storage = ConcurrentHashMap()

        root = Block.newRoot()
        leaves = ConcurrentHashMap()
        leaves.put(root.hash, root);
        hashToBlock = hashMapOf(Pair(root.hash, root))
        this.owner = owner
    }

    private fun addEnd(block: Block): Boolean {
        treeLock.withLock {
            val leaf: Block? = leaves.get(block.previousHash)
            if (leaf != null) {
                leaf.addChild(block);
                leaves.remove(leaf.hash)
                leaves.put(block.hash, block)
                return successfulNewBlock(block);
            }
        }
        return false;
    }

    fun add(block: Block, verified: Boolean = block.verify()): Boolean {
        if (!verified) return false;
        treeLock.withLock {
            if (addEnd(block)) return successfulNewBlock(block)
            return add(block, findBlockForHash(block.previousHash), verified)
        }
    }

    fun add(block: Block, parent: Block?, verified: Boolean = block.verify()): Boolean {
        if (!verified) return false;
        if (parent == null) {
            return store(block)
        } else {
            treeLock.withLock {
                if (parent.isLeaf()) return addEnd(block)
                if (parent.getChildren().map { child -> (child as Block).hash }.contains(block.hash)) return false
                parent.addChild(block)
                leaves.put(block.hash, block)
                return successfulNewBlock(block)
            }
        }
    }

    fun addAll(newBlocks: List<Block>): List<Boolean> {
        val parent: Block? = findBlockForHash(newBlocks.first().previousHash)
        return addAll(parent, newBlocks)
    }

    fun addAll(parent: Block?, newBlocks: List<Block>): List<Boolean> {
        val results: ArrayList<Boolean> = arrayListOf()
        var previous = parent
        treeLock.withLock {
            for (block in newBlocks) {
                results.add(add(block, previous))
                previous = block;
            }
            return results
        }
    }

    private fun successfulNewBlock(block: Block): Boolean {
        treeLock.withLock {
            hashToBlock.put(block.hash, block)
            resyncStorage()
            EventBus.dispatch(Event(EventType.MODIFY_BLOCKCHAIN, owner, block))
            return true
        }
    }

    private fun store(block: Block): Boolean {
        treeLock.withLock {
            EventBus.dispatch(Event(EventType.ADD_TO_STORAGE, owner, block))
            return storage.putIfAbsent(block.hash, block) == null
        }
    }

    fun findBlockForHash(hash: String): Block? {
        treeLock.withLock {
            return hashToBlock.get(hash)
        }
    }

    fun clearStorage() {
        treeLock.withLock {
            val removed = storage.values.toList()
            storage.clear()
            if (removed.isNotEmpty()) {
                EventBus.dispatch(Event(EventType.CLEAR_STORAGE, owner, removed))
            }
        }
    }

    fun resyncStorage() {
        treeLock.withLock {
            val blocks = storage.values;
            var recheck = true;
            while (recheck) {
                recheck = false;
                for (block in blocks) {
                    if (addEnd(block)) {
                        storage.remove(block.hash)
                        recheck = true;
                        EventBus.dispatch(Event(EventType.ADD_FROM_STORAGE, owner, block))
                    }
                }
            }
        }
    }

    fun getLongestHeight(): Int {
        treeLock.withLock {
            return leaves.values.map { leaf -> leaf.height }.max()!!
        }
    }

    fun getLastBlockInLongestChain(): Block {
        treeLock.withLock {
            return leaves.values.maxBy { leaf -> leaf.height }!!
        }
    }

    fun getBlocksInLongestChain(): LinkedList<TreeNode> {
        treeLock.withLock {
            return getLastBlockInLongestChain().searchUp()
        }
    }

    fun getAllBlocks(): ArrayList<Block> {
        val nodes: ArrayList<Block> = ArrayList()
        root.bfs({ i, n ->
            nodes.add(n as Block)
        })
        return nodes
    }

    fun prune(): List<TreeNode> {
        treeLock.withLock {
            var sideChains: List<TreeNode> = getLastBlockInLongestChain().prune()
            sideChains = sideChains.flatMap({ node: TreeNode ->
                val nodes: HashSet<TreeNode> = HashSet()
                node.bfs({ i, n ->
                    nodes.add(n as Block)
                    leaves.remove(n.hash)
                })
                nodes.toList()
            })
            return sideChains
        }
    }

}