package blockchain
import events.Event
import events.EventBus
import events.EventType
import mu.KLogging


class Wallet(connectTo: List<Node> = emptyList()) : Node("wallet", connectTo = connectTo) {
    companion object: KLogging()

    override fun receiveBlock(block: Block) {
        logger.info { "Received block ${block.uuid}. Ignoring." }
    }

    override fun toString(): String {
        return "publicKey: $publicKey\r\nprivateKey: $privateKey"
    }
    override fun receiveTransaction(transaction: Transaction) {
        val verified = transaction.verify();
        if (verified) {
            if (getMempool().addIfNotPresent(transaction)) {
                EventBus.dispatch(Event(EventType.RECEIVE_TRANSACTION, this.publicKey, transaction))
                getPeers().values.forEach { peer -> peer.receiveTransaction(transaction.clone()) }
            }
        }
    }
}