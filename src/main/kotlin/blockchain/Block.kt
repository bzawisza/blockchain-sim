package blockchain

import crypto.Hash
import events.EventBus
import events.Event
import events.EventType
import structures.TreeNode
import java.util.*

class Block: TreeNode {
    private val timestamp: Long
    var data: List<Transaction>
    var previousHash: String
    val difficultyPrefix: String
    val uuid: UUID
    var hash = ""
    var nonce: Long = 0L

    private constructor() {
        timestamp = 0L;
        data = emptyList();
        previousHash = ""
        difficultyPrefix = ""
        uuid = UUID.fromString("00000000-0000-0000-0000-000000000000");
    }

    companion object {
        fun newRoot():Block {
            val origin = Block().updateHash()
            EventBus.dispatch(Event(EventType.MINE_BLOCK, "", origin))
            return origin
        }
    }

    constructor(timestamp: Long, data: List<Transaction>, previousHash: String, difficultyPrefix: String, uuid: UUID = UUID.randomUUID()) {
        this.timestamp = timestamp
        this.data = data
        this.previousHash = previousHash
        this.difficultyPrefix = difficultyPrefix
        this.uuid = uuid
    }

    fun verify(verifyTransactions: Boolean = true): Boolean {
        if (verifyTransactions) {
            val verifiedTransactions = data.map { transaction -> transaction.verify() };
            return hash.startsWith(difficultyPrefix) && hash.isNotEmpty() && (if (verifiedTransactions.isNotEmpty()) verifiedTransactions.reduce{ acc, ver -> acc && ver } else true)
        } else {
            return hash.startsWith(difficultyPrefix) && hash.isNotEmpty()
        }
    }

    override fun clone(): Block {
        val block = Block(timestamp, data, previousHash, difficultyPrefix, uuid)
        block.data = block.data.map { trans -> trans.clone() }
        block.hash = this.hash;
        block.nonce = this.nonce;
        block.updateHash()
        block.height = this.height
        block.setParent(this.getParent())
        block.children = ArrayList(block.children.map { child -> child.clone() })
        block.children.forEach { child -> child.setParent(block) }
        return block
    }

    fun verify(previousHash: String): Boolean {
        this.previousHash = previousHash
        return verify()
    }

    fun updateHash(): Block {
        hash = Hash().doHash("$timestamp$data$uuid$previousHash$nonce")
        return this
    }

    override fun toString(): String {
        return "Block(timestamp=$timestamp, data=$data, previousHash='$previousHash', difficultyPrefix='$difficultyPrefix', uuid=$uuid, hash='$hash', nonce=$nonce)"
    }

    override fun hashCode(): Int {
        var result = timestamp.hashCode()
        result = 31 * result + data.hashCode()
        result = 31 * result + previousHash.hashCode()
        result = 31 * result + difficultyPrefix.hashCode()
        result = 31 * result + uuid.hashCode()
        result = 31 * result + hash.hashCode()
        result = 31 * result + nonce.hashCode()
        return result
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Block

        if (timestamp != other.timestamp) return false
        if (data != other.data) return false
        if (previousHash != other.previousHash) return false
        if (difficultyPrefix != other.difficultyPrefix) return false
        if (uuid != other.uuid) return false
        if (hash != other.hash) return false
        if (nonce != other.nonce) return false

        return true
    }

}