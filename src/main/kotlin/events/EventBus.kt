package events

import mu.KLogging
import mu.KotlinLogging
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue

object EventBus {
    private val logger = KotlinLogging.logger {}
    private val subscriptions: ConcurrentHashMap<EventType, ConcurrentLinkedQueue<(Event) -> Unit>> = ConcurrentHashMap()

    fun dispatch(event: Event) {
        logger.trace { "dispatch ${event}" }
        subscriptions.get(event.eventType)?.forEach { cb -> cb(event) }
    }

    fun subscribe(eventType: EventType, cb: (Event) -> Unit) {
        subscriptions.putIfAbsent(eventType, ConcurrentLinkedQueue())
        subscriptions.get(eventType)!!.add(cb)
    }
    fun unsubscribe(eventType: EventType, cb: (Event) -> Unit) {
        val cbs = subscriptions.get(eventType)
        if (cbs != null) {
            cbs.remove(cb)
        }
    }

}
