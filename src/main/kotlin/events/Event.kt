package events

class Event {
    val sender: String
    val eventType: EventType
    val data: Any?
    constructor(eventType: EventType, sender: String): this(eventType, sender, null)
    constructor(eventType: EventType, sender: String, data: Any?) {
        this.eventType = eventType
        this.sender = sender
        this.data = data
    }

    override fun toString(): String {
        return "Event(sender='$sender', eventType=$eventType, data=$data)"
    }


}
