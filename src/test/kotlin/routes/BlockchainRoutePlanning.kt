package routes

import blockchain.Block
import events.Event
import events.EventBus
import events.EventType
import org.jgrapht.GraphPath
import tracking.Watcher
import java.util.*


fun setup(p: Int): ArrayList<RoutePlanner> {
    val planners = arrayListOf<RoutePlanner>()
    for (i in 1..p) {
        val planner = RoutePlanner(i, 3, 3, 1, 10000)
        planners.forEach{otherPlanners -> planner.connect(otherPlanners)}
        planners.add(planner)

        var cb = { event:Event -> Unit}
        cb = @Synchronized { event:Event ->
            val existing = planner.getBlockchain().getBlocksInLongestChain().flatMap { block -> (block as Block).data }.map { transaction -> (transaction.value as Pair<RoutePlanner, GraphPath<RoadTimeFragment, RoadEdge>>).first }
            if (!existing.contains(planner)) {
                planner.reconstructGraph()
                planner.newPath()
            } else if (planner.transaction != null && Watcher.percentageNodesHasTransaction(planner.transaction!!.uuid) == 1.0) {
                EventBus.unsubscribe(EventType.CONSENSUS, cb)
                EventBus.unsubscribe(EventType.ADD_FROM_STORAGE, cb)
                EventBus.unsubscribe(EventType.MINE_BLOCK, cb)
                EventBus.unsubscribe(EventType.RECEIVE_BLOCK, cb)
            }
        }
        EventBus.subscribe(EventType.CONSENSUS, cb)
        EventBus.subscribe(EventType.ADD_FROM_STORAGE, cb)
        EventBus.subscribe(EventType.MINE_BLOCK, cb)
        EventBus.subscribe(EventType.RECEIVE_BLOCK, cb)
        planner.newPath()
    }
    return planners
}

/*
    Because of the way I'm subscribing to events, locking, and adding new transactions, theres a very small chance of deadlock.
    This is caused by simulating this specific scenario and not the blockchain itself.
 */
fun main(args: Array<String>) {
    Watcher // initialize Watcher singleton.
    val numberOfPlanners = 7
    val planners = setup(numberOfPlanners)

    // Printing of what's currently in the blockchain
    var done = false
    planners.forEach {
        Thread {
            while (!done) {
                println(it.getBlockchain().getBlocksInLongestChain().map { block -> (block as Block).uuid.toString().takeLast(6) })
                Thread.yield()
                Thread.sleep(1000)
            }
        }.start()
    }

    // Runs in parallel
    planners.map { planner ->
        val t = Thread {
            planner.mineblocks{ planner.getBlockchain().getLongestHeight() == numberOfPlanners+1}
        }
        t.start()
        t
    }.forEach { t -> t.join() }
    done = true

    // Print the results!
    planners.first().consensus(force = true)
    println(planners.first().getBlockchain().getBlocksInLongestChain().map { block ->
        val block = (block as Block)
        if (block.data.isNotEmpty()) {
            val data = ((block as Block).data.first().value as Pair<RoutePlanner, GraphPath<RoadTimeFragment, RoadEdge>>)
            data.first.takePath(data.second)
            "Vehicle ${data.first.vehicle.id}: Route: ${data.second.vertexList.joinToString { "$it" }} Overall Time: ${data.first.vehicle.timeToTarget}"
        } else {
            null
        }
    }.joinToString { "\n$it" })

    println("Elapsed Time: ${Watcher.getTimePassed()} ms")
}