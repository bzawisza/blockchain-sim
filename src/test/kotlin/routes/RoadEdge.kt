package routes

import org.jgrapht.graph.DefaultWeightedEdge

class RoadEdge: DefaultWeightedEdge() {
    public override fun getWeight(): Double {
        return super.getWeight()
    }

    override fun toString(): String {
        return "${this.weight}";
    }
}