package routes

class RoadTimeFragment {
    val name: String
    val id: Int
    val fragmentId: Int
    val timeToTravelForOneVehicle: Double
    var space: Vehicle? = null
    constructor(id: Int, fragmentId: Int, name: String, timeToTravelForOneVehicle: Double) {
        this.id = id
        this.name = name
        this.fragmentId = fragmentId
        this.timeToTravelForOneVehicle = timeToTravelForOneVehicle
    }
    fun isTaken(): Boolean {
        return space == null
    }
    fun taken(vehicle: Vehicle) {
        this.space = vehicle
    }

//    override fun toString(): String {
//        return "${id*timeToTravelForOneVehicle} $fragmentId $name |" + if (space == null) " |" else space.toString() + "|"
//    }
    override fun toString(): String {
        if (name.equals("start") || name.equals("destination"))
            return "-";
        return "$name$fragmentId(${id*timeToTravelForOneVehicle})"
    }
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as RoadTimeFragment

        if (name != other.name) return false
        if (id != other.id) return false
        if (fragmentId != other.fragmentId) return false
        if (timeToTravelForOneVehicle != other.timeToTravelForOneVehicle) return false
//        if (space != other.space) return false

        return true
    }

    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + id
        result = 31 * result + fragmentId
        result = 31 * result + timeToTravelForOneVehicle.hashCode()
//        result = 31 * result + (space?.hashCode() ?: 0)
        return result
    }


}