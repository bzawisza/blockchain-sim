package routes

import org.jgrapht.graph.SimpleDirectedWeightedGraph
import java.util.*

class RoadFragment {
    val name: String
    val id: Int
    val timeFragments: ArrayList<RoadTimeFragment>
    var fragmentCount: Int
    val type: String
    val timeToTravelForOneVehicle: Double

    constructor(id: Int, name:String, timeToTravelForOneVehicle: Double, simulationTime: Double, graph: SimpleDirectedWeightedGraph<RoadTimeFragment, RoadEdge>, type:String = "") {
        this.id = id
        this.fragmentCount = 0
        this.timeFragments = arrayListOf()
        this.name = name
        this.timeToTravelForOneVehicle = timeToTravelForOneVehicle
        this.type = type
        if (type.equals("stop") || type.equals("start")) { // destination
            addTimeFragment(graph, simulationTime)
        } else {
            for (i in 1..Math.floor(simulationTime / timeToTravelForOneVehicle).toInt()) {
                addTimeFragment(graph)
            }
        }
    }

    private fun addTimeFragment(graph: SimpleDirectedWeightedGraph<RoadTimeFragment, RoadEdge>, timeToTravelForOneVehicle: Double = this.timeToTravelForOneVehicle) {
        val fragment = RoadTimeFragment(fragmentCount++, id, name, timeToTravelForOneVehicle)
        graph.addVertex(fragment)
        if (timeFragments.isNotEmpty()) {
            val e1 = graph.addEdge(timeFragments.last(), fragment)
            if (e1 != null) {
                graph.setEdgeWeight(e1, timeToTravelForOneVehicle)
            }
        }
        timeFragments.add(fragment)
    }

    override fun toString(): String {
        return "$id (" + timeFragments.map { fragment -> fragment.toString() }.reduce { a, b -> if  (!a.startsWith("[")) "[$a] [$b]" else "$a [$b]"} + ")"
    }
}