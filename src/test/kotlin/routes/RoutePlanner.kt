package routes

import blockchain.Block
import blockchain.Miner
import blockchain.Transaction
import org.jgrapht.GraphPath
import org.jgrapht.alg.shortestpath.DijkstraShortestPath

class RoutePlanner(id: Int, difficulty: Int, sleepRange: Int, transactions_per_block: Int, consensusTime: Int) : Miner(difficulty, sleepRange, transactions_per_block, consensusTime) {
    var proposedPath: GraphPath<RoadTimeFragment, RoadEdge>? = null
    val vehicle: Vehicle
    lateinit var sim: RouteSim
    lateinit var start: RoadFragment
    lateinit var stop: RoadFragment
    lateinit var dijkstras: DijkstraShortestPath<RoadTimeFragment, RoadEdge>
    var transaction: Transaction? = null

    init {
        genGraph()
        vehicle = Vehicle(id, start.timeFragments.first(), stop.timeFragments.last())
    }

    override fun cleanupTransactions() {
        // Techinically we should be removing all INTERSECTING routes... but this is fine for this SPECIFIC test
        val transactions = getMempool().getPendingTransactions()
        val completeRoutes = getBlockchain().getBlocksInLongestChain().flatMap { block -> (block as Block).data }.map { transaction -> (transaction.value as Pair<*, GraphPath<*, *>>).second.vertexList }
        transactions.forEach{
            if (completeRoutes.contains((it.value as Pair<*, GraphPath<*, *>>).second.vertexList)) {
                getMempool().deleteTransaction(it)
            }
        }
    }

    private fun genGraph() {
        sim = RouteSim(15.0, 1.0)
        start = sim.addStart("start")
        val roadA = sim.addRoad("a", 1.0, 3.0)
        val roadB = sim.addRoad("b", speedLimit = .5, length = 2.0)
        val roadC = sim.addRoad("c", speedLimit = .5, length = 2.0)
        stop = sim.addStop("destination")

        sim.connectRoads(start, roadA.fragments.first())
        sim.connectRoads(roadA.fragments.last(), roadB.fragments.first())
        sim.connectRoads(roadA.fragments.last(), roadC.fragments.first())
        sim.connectRoads(roadB.fragments.last(), stop)
        sim.connectRoads(roadC.fragments.last(), stop)

        dijkstras = DijkstraShortestPath(sim.graph)
    }

    fun reconstructGraph() {
        genGraph()
        getBlockchain().getBlocksInLongestChain().forEach { node ->
            val data = (node as Block).data
            if (data.isNotEmpty()) {
                val value = data.first().value
                takePath((value as Pair<RoutePlanner, GraphPath<RoadTimeFragment, RoadEdge>>).second)
            }
        }
    }

    fun newPath() {
        proposedPath =  dijkstras.getPath(start.timeFragments.first(), stop.timeFragments.last())
        this.transaction = this.create_transaction(this.publicKey, Pair(this, this.proposedPath))

    }

    fun takePath(path: GraphPath<RoadTimeFragment, RoadEdge>) {
        sim.takePath(vehicle, path)
    }
}