package routes

import com.jgraph.layout.JGraphFacade
import com.jgraph.layout.hierarchical.JGraphHierarchicalLayout
import org.jgraph.JGraph
import org.jgrapht.ext.JGraphModelAdapter
import org.jgrapht.graph.SimpleDirectedWeightedGraph
import java.util.*
import org.jgrapht.GraphPath
import org.jgrapht.alg.shortestpath.DijkstraShortestPath
import javax.swing.JFrame
import javax.swing.JScrollPane


class RouteSim {
    val graph: SimpleDirectedWeightedGraph<RoadTimeFragment, RoadEdge>
    val roads: ArrayList<Road>
    val simulationTime: Double
    val vehicleSize: Double

    constructor(simulationTime: Double, vehicleSize: Double) {
        graph = SimpleDirectedWeightedGraph(RoadEdge::class.java)
        this.simulationTime = simulationTime
        this.roads = arrayListOf()
        this.vehicleSize = vehicleSize
    }

    fun addRoad(name: String, speedLimit: Double, length: Double): Road {
        val road = Road(this, name, speedLimit, length, vehicleSize, this.simulationTime, this.graph)
        roads.add(road)
        return road
    }

    fun addStop(name: String): RoadFragment {
        return RoadFragment(0, name, 0.0, simulationTime, this.graph, type="stop")
    }

    fun addStart(name: String): RoadFragment {
        return RoadFragment(0, name, 0.0, simulationTime, this.graph, type="start")
    }

    fun connectRoads(fragmenta: RoadFragment, fragmentb: RoadFragment) {

        if (fragmenta.type.equals("start")) {
            fragmentb.timeFragments.forEachIndexed { i, timeFragmentb ->
                val e = this.graph.addEdge(fragmenta.timeFragments.first(), timeFragmentb)
                this.graph.setEdgeWeight(e, i*fragmentb.timeToTravelForOneVehicle)
            }
        } else {
            fragmenta.timeFragments.forEachIndexed { i, roadTimeFragment ->
                val timeLeftSource = fragmenta.timeToTravelForOneVehicle * (i + 1)
                var timeFragmentIndexInTarget = Math.ceil(timeLeftSource / fragmentb.timeToTravelForOneVehicle).toInt()
                if (fragmentb.type.equals("stop")) {
                    val e = this.graph.addEdge(roadTimeFragment, fragmentb.timeFragments[fragmentb.fragmentCount - 1])
                    this.graph.setEdgeWeight(e, fragmenta.timeToTravelForOneVehicle)
                } else {
                    var timeToTarget = fragmenta.timeToTravelForOneVehicle//fragmentb.timeToTravelForOneVehicle * timeFragmentIndexInTarget - fragmenta.timeToTravelForOneVehicle * i
                    while (timeFragmentIndexInTarget < fragmentb.timeFragments.size && timeToTarget <= 2 * fragmenta.timeToTravelForOneVehicle) {
                        timeToTarget = fragmenta.timeToTravelForOneVehicle//fragmentb.timeToTravelForOneVehicle * timeFragmentIndexInTarget - fragmenta.timeToTravelForOneVehicle * i
                        val connectingFragment = fragmentb.timeFragments[timeFragmentIndexInTarget]
                        val e = this.graph.addEdge(roadTimeFragment, connectingFragment)
                        this.graph.setEdgeWeight(e, timeToTarget)
                        if (fragmenta.timeToTravelForOneVehicle <= fragmentb.timeToTravelForOneVehicle) {
                            break;
                        }
                        timeFragmentIndexInTarget++
                    }
                }
            }
        }
    }

    fun takePath(vehicle: Vehicle, path: GraphPath<RoadTimeFragment, RoadEdge>) {
        vehicle.timeToTarget = path.edgeList.map { roadEdge: RoadEdge? -> roadEdge!!.weight }.sum()
        val edgesToRemove: List<RoadEdge> = path.vertexList.flatMap{ roadTimeFragment ->
            if (roadTimeFragment.fragmentId != 0) {
                roadTimeFragment.space = vehicle
                val incomingEdges = graph.incomingEdgesOf(roadTimeFragment)
                incomingEdges
            } else {
                emptyList<RoadEdge>()
            }
        }
//        println(path.vertexList)
        graph.removeAllEdges(edgesToRemove)
    }

    fun removeOtherRoads(path: GraphPath<RoadTimeFragment, RoadEdge>) {
        val names = path.vertexList.map { roadTimeFragment -> roadTimeFragment.name + roadTimeFragment.fragmentId }.toHashSet()
        val toRemove = graph.vertexSet().filter { roadTimeFragment -> !names.contains(roadTimeFragment.name + roadTimeFragment.fragmentId) }
        graph.removeAllVertices(toRemove)
    }
}

fun fullExample() {
    val sim = RouteSim(26.0, 1.0)
    val start = sim.addStart("start")
    val roadA = sim.addRoad("a", 1.0, 3.0)
    val roadB = sim.addRoad("b", speedLimit = .5, length = 2.0)
    val roadC = sim.addRoad("c", speedLimit = .5, length = 2.0)
    val stop = sim.addStop("destination")

    sim.connectRoads(start, roadA.fragments.first())
    sim.connectRoads(roadA.fragments.last(), roadB.fragments.first())
    sim.connectRoads(roadA.fragments.last(), roadC.fragments.first())
    sim.connectRoads(roadB.fragments.last(), stop)
    sim.connectRoads(roadC.fragments.last(), stop)

    val vehicles: ArrayList<Vehicle> = arrayListOf()
    val routes = ArrayList<GraphPath<RoadTimeFragment, RoadEdge>>()

    val vehicle1 = Vehicle(1, start.timeFragments.first(), stop.timeFragments.last())
    var path = DijkstraShortestPath.findPathBetween(sim.graph, vehicle1.start, vehicle1.destination)
    sim.takePath(vehicle1, path)
    sim.removeOtherRoads(path)
    vehicles.add(vehicle1)
    routes.add(path)

    for (i in 2..7) {
        val vehicle = Vehicle(i, start.timeFragments.first(), stop.timeFragments.last())
        path = DijkstraShortestPath.findPathBetween(sim.graph, vehicle.start, vehicle .destination)
        routes.add(path)
        sim.takePath(vehicle, path)
        vehicles.add(vehicle)
    }

    vehicles.forEachIndexed { i, vehicle ->
        println("Vehicle ${i+1}: Route: [${routes.get(i).vertexList.joinToString { "$it" }}] Time: ${vehicle.timeToTarget}")
    }

}

fun test(): RouteSim {
    val sim = RouteSim(4.0, 1.0)
    val start = sim.addStart("start")
    val roadA = sim.addRoad("a", 1.0, 2.0)
    val roadB = sim.addRoad("b", speedLimit = .5, length = 2.0)
    val roadC = sim.addRoad("c", speedLimit = .5, length = 2.0)
    val stop = sim.addStop("destination")

    sim.connectRoads(start, roadA.fragments.first())
    sim.connectRoads(roadA.fragments.last(), roadB.fragments.first())
    sim.connectRoads(roadA.fragments.last(), roadC.fragments.first())
    sim.connectRoads(roadB.fragments.last(), stop)
    sim.connectRoads(roadC.fragments.last(), stop)

    return sim
}


fun display(sim: RouteSim) {
    val jgraph = JGraph(JGraphModelAdapter(sim.graph))
    val frame = JFrame()
    val layout = JGraphHierarchicalLayout()
    val graphFacade: JGraphFacade = JGraphFacade(jgraph);
    layout.run(graphFacade)
    jgraph.graphLayoutCache.edit(graphFacade.createNestedMap(true, true))
    val pane = JScrollPane(jgraph)
    frame.contentPane.add(pane)
    frame.defaultCloseOperation = JFrame.EXIT_ON_CLOSE
    frame.pack()
    frame.isVisible = true
}
fun main(args: Array<String>) {
    fullExample()
//    display(test())
}