package routes

import blockchain.Miner
import org.jgrapht.GraphPath

class Vehicle {
    val start: RoadTimeFragment
    val destination: RoadTimeFragment
    val id: Int
    var timeToTarget: Double

    constructor(id: Int, start: RoadTimeFragment, destination: RoadTimeFragment) {
        this.id = id
        this.start = start
        this.destination = destination
        this.timeToTarget = -1.0
    }

    override fun toString(): String {
        return id.toString()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Vehicle

        if (start != other.start) return false
        if (destination != other.destination) return false
        if (id != other.id) return false
        if (timeToTarget != other.timeToTarget) return false

        return true
    }

    override fun hashCode(): Int {
        var result = start.hashCode()
        result = 31 * result + destination.hashCode()
        result = 31 * result + id
        result = 31 * result + timeToTarget.hashCode()
        return result
    }


}