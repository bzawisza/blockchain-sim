package routes

import org.jgrapht.graph.SimpleDirectedWeightedGraph
import java.lang.Math.floor
import java.util.*

class Road {
    val name: String
    val timeToTravelForOneVehicle: Double
    val vehicleSize: Double
    val speedLimit: Double
    val length: Double
    val fragments: ArrayList<RoadFragment>
    val fragmentCount: Int
    val graph: SimpleDirectedWeightedGraph<RoadTimeFragment, RoadEdge>

    constructor(sim: RouteSim, name: String, speedLimit:Double, length: Double, vehicleSize: Double, simulationTime: Double, graph: SimpleDirectedWeightedGraph<RoadTimeFragment, RoadEdge>) {
        this.name = name
        this.speedLimit = speedLimit
        this.vehicleSize = vehicleSize
        this.timeToTravelForOneVehicle = vehicleSize/speedLimit
        this.length = length
        this.fragments = arrayListOf()
        this.graph = graph
        this.fragmentCount = floor(length/vehicleSize).toInt()
        for (i in 1..this.fragmentCount) {
            val fragment = RoadFragment(i, name, timeToTravelForOneVehicle, simulationTime, graph)
            fragments.add(fragment)
        }
        fragments.forEachIndexed {i, roadFragment ->
            if (i < fragments.size-1) {
                val next = fragments[i+1]
                sim.connectRoads(roadFragment, next)
            }
        }
    }



    override fun toString(): String {
        return "$name [" + fragments.map { fragment -> fragment.toString() }.reduce { a, b -> "$a, $b"} + "]"
    }
}