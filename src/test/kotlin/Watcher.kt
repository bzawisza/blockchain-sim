import blockchain.Block
import org.assertj.core.api.Assertions.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import blockchain.Miner
import blockchain.Wallet
import tracking.Watcher
import java.util.*

class Watcher: Spek({
    // singleton. we need to instantiate it.
    beforeEachTest {
        Watcher.reset()
    }

    given("Working simulated blockchain implementation") {
        on("Creating nodes") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            it("should keep track of all created nodes") {
                assertThat(Watcher.getNodeCount()).isEqualTo(2)
            }
        }

        on("nodes connecting to each other") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            it("should record timestamps of when nodes connected to each other") {
                assertThat(Watcher.getOpenConnections().keys).contains(wallet.publicKey)
                assertThat(Watcher.getOpenConnections().keys).contains(miner.publicKey)
                assertThat(Watcher.getOpenConnections().get(miner.publicKey)!!.keys).contains(wallet.publicKey)
                assertThat(Watcher.getOpenConnections().get(wallet.publicKey)!!.keys).contains(miner.publicKey)
                assertThat(Watcher.getOpenConnections().get(wallet.publicKey)!!.get(miner.publicKey))
                        .isLessThanOrEqualTo(Watcher.getOpenConnections().get(miner.publicKey)!!.get(wallet.publicKey)!!)
            }
        }

        on("nodes disconnecting from each other") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            miner.disconnect(wallet)
            it("should get removed from open connections") {
                assertThat(Watcher.getOpenConnections().keys).contains(wallet.publicKey)
                assertThat(Watcher.getOpenConnections().keys).contains(miner.publicKey)
                assertThat(Watcher.getOpenConnections().get(miner.publicKey)!!.keys).doesNotContain(wallet.publicKey)
                assertThat(Watcher.getOpenConnections().get(wallet.publicKey)!!.keys).doesNotContain(miner.publicKey)
            }
            it("should add in a new entry to closed connections") {
                assertThat(Watcher.getClosedConnections().keys).contains(wallet.publicKey)
                assertThat(Watcher.getClosedConnections().keys).contains(miner.publicKey)
                assertThat(Watcher.getClosedConnections().get(wallet.publicKey)!!.keys).contains(miner.publicKey)
                assertThat(Watcher.getClosedConnections().get(miner.publicKey)!!.keys).contains(wallet.publicKey)
                assertThat(Watcher.getClosedConnections().get(wallet.publicKey)!!.get(miner.publicKey)).hasSize(1)
                assertThat(Watcher.getClosedConnections().get(miner.publicKey)!!.get(wallet.publicKey)).hasSize(1)
                var pair = Watcher.getClosedConnections().get(wallet.publicKey)!!.get(miner.publicKey)!!.first()
                assertThat(pair.first).isLessThanOrEqualTo(pair.second)
                pair = Watcher.getClosedConnections().get(miner.publicKey)!!.get(wallet.publicKey)!!.first()
                assertThat(pair.first).isLessThanOrEqualTo(pair.second)
            }
        }

        on("nodes disconnecting and connecting from each other adhocly") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            miner.disconnect(wallet)
            miner.connect(wallet)
            miner.disconnect(wallet)
            it("should get removed from open connections") {
                assertThat(Watcher.getOpenConnections().keys).contains(wallet.publicKey)
                assertThat(Watcher.getOpenConnections().keys).contains(miner.publicKey)
                assertThat(Watcher.getOpenConnections().get(miner.publicKey)!!.keys).doesNotContain(wallet.publicKey)
                assertThat(Watcher.getOpenConnections().get(wallet.publicKey)!!.keys).doesNotContain(miner.publicKey)
            }
            it("should add in a new entry to closed connections") {
                assertThat(Watcher.getClosedConnections().keys).contains(wallet.publicKey)
                assertThat(Watcher.getClosedConnections().keys).contains(miner.publicKey)
                assertThat(Watcher.getClosedConnections().get(wallet.publicKey)!!.keys).contains(miner.publicKey)
                assertThat(Watcher.getClosedConnections().get(miner.publicKey)!!.keys).contains(wallet.publicKey)
                assertThat(Watcher.getClosedConnections().get(wallet.publicKey)!!.get(miner.publicKey)).hasSize(2)
                assertThat(Watcher.getClosedConnections().get(miner.publicKey)!!.get(wallet.publicKey)).hasSize(2)
                Watcher.getClosedConnections().get(wallet.publicKey)!!.get(miner.publicKey)!!.forEach{pair ->
                    assertThat(pair.first).isLessThanOrEqualTo(pair.second)
                }
                Watcher.getClosedConnections().get(miner.publicKey)!!.get(wallet.publicKey)!!.forEach{pair ->
                    assertThat(pair.first).isLessThanOrEqualTo(pair.second)
                }
            }
        }
        on("initial connection consensus") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            it("should have an entry for the inital consensus") {
                assertThat(Watcher.getConsensi().keys).contains(wallet.publicKey)
                assertThat(Watcher.getConsensi().keys).contains(miner.publicKey)
                assertThat(Watcher.getConsensi().get(wallet.publicKey)!!).hasSize(1)
                assertThat(Watcher.getConsensi().get(miner.publicKey)!!).hasSize(1)
                assertThat(Watcher.getConsensi().get(wallet.publicKey)!!.first().first).isEqualTo(wallet.publicKey)
                assertThat(Watcher.getConsensi().get(miner.publicKey)!!.first().first).isEqualTo(miner.publicKey)
            }
        }

        on ("consensus after mining blocks") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            it("should show that a longer chain was received by another node") {
                wallet.create_transaction(miner.publicKey, "1")
                miner.mineblocks { i -> i == 1 }
                wallet.consensus(force=true)
                it("should keep track of where the results of consensus came from") {
                    assertThat(Watcher.getConsensi().get(miner.publicKey)!!).hasSize(1)
                    assertThat(Watcher.getConsensi().get(wallet.publicKey)!!).hasSize(2)
                    assertThat(Watcher.getConsensi().get(wallet.publicKey)!!.first().first).isEqualTo(miner.publicKey)
                }
            }
        }

        on("create new transaction") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            val t1 = wallet.create_transaction(miner.publicKey, "1")!!
            val t2 = wallet.create_transaction(miner.publicKey, "1")!!
            it("should record the transaction") {
                assertThat(Watcher.getTransactions().keys).contains(t1.uuid)
                assertThat(Watcher.getTransactions().keys).contains(t2.uuid)
            }
        }

        on ("receive transaction") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            val t1 = wallet.create_transaction(miner.publicKey, "1")!!
            it("should store a list of all received transactions") {
                assertThat(Watcher.getReceivedTransactions().keys).contains(miner.publicKey)
                assertThat(Watcher.getReceivedTransactions().keys).contains(wallet.publicKey)
                assertThat(Watcher.getReceivedTransactions().get(miner.publicKey)!!.map{t->t.uuid}).contains(t1.uuid)
            }
        }

        on ("node discovers new transaction when connecting") {
            val wallet: Wallet = Wallet();
            val wallet2: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            val t1 = wallet.create_transaction(miner.publicKey, "1")!!
            wallet2.connect(miner)
            it("should store a list of all synced transactions") {
                assertThat(Watcher.getSyncedTransactions().keys).contains(wallet2.publicKey)
                assertThat(Watcher.getSyncedTransactions().get(wallet2.publicKey)!!.map{t->t.uuid}).contains(t1.uuid)
            }
        }

        on ("mine new block") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            it ("should keep track of the new block") {
                val block = miner.getBlockchain().getLastBlockInLongestChain()
                assertThat(Watcher.getBlocks().keys).contains(block.uuid)
                assertThat(Watcher.getBlocks().keys).hasSize(2)
            }
        }

        on ("receiving a block") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            miner2.connect(miner)
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            it("should keep track of all received blocks") {
                assertThat(Watcher.getReceivedBlocks().keys).doesNotContain(wallet.publicKey)
                assertThat(Watcher.getReceivedBlocks().keys).doesNotContain(miner.publicKey)
                assertThat(Watcher.getReceivedBlocks().keys).contains(miner2.publicKey)
                assertThat(Watcher.getReceivedBlocks().get(miner2.publicKey)).hasSize(1)
                val uuid = miner.getBlockchain().getLastBlockInLongestChain().uuid
                assertThat(Watcher.getReceivedBlocks().get(miner2.publicKey)!!.first().uuid).isEqualTo(uuid)
            }
        }

        on ("removing a single block due to consensus") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            wallet.connect(miner2)

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }

            miner2.mineblocks { i -> i == 1 }
            miner2.connect(miner)
            it ("should keep track of the number of blocks that were dropped") {
                assertThat(Watcher.getDroppedBlocks().keys).contains(miner2.publicKey)
                if (Watcher.getDroppedBlocks().containsKey(miner.publicKey)) {
                    assertThat(Watcher.getDroppedBlocks().get(miner.publicKey)).isEqualTo(0)
                }
                if (Watcher.getDroppedBlocks().containsKey(wallet.publicKey)) {
                    assertThat(Watcher.getDroppedBlocks().get(wallet.publicKey)).isEqualTo(0)
                }
                assertThat(Watcher.getDroppedBlocks().get(miner2.publicKey)).isEqualTo(1)
            }
        }

        on ("removing a multiple block due to consensus") {
            val wallet: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            wallet.connect(miner2)
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            miner2.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            miner2.mineblocks { i -> i == 1 }
            miner2.connect(miner)
            it ("should keep track of the number of blocks that were dropped") {
                assertThat(Watcher.getDroppedBlocks().keys).contains(miner2.publicKey)
                if (Watcher.getDroppedBlocks().containsKey(miner.publicKey)) {
                    assertThat(Watcher.getDroppedBlocks().get(miner.publicKey)).isEqualTo(0)
                }
                if (Watcher.getDroppedBlocks().containsKey(wallet.publicKey)) {
                    assertThat(Watcher.getDroppedBlocks().get(wallet.publicKey)).isEqualTo(0)
                }
                assertThat(Watcher.getDroppedBlocks().get(miner2.publicKey)).isEqualTo(2)
            }
        }

        on ("reintroduce transactions") {
            val wallet: Wallet = Wallet();
            val wallet2: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
            wallet2.connect(miner2)
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            val t1 = wallet2.create_transaction(miner2.publicKey, "1")!!
            miner2.mineblocks { i -> i == 1 }
            miner2.connect(miner)
            it ("should keep track of how many times a transactino was introduced into the mempool") {
                assertThat(Watcher.getReintroducedTransactions().keys).contains(t1.uuid)
                assertThat(Watcher.getReintroducedTransactions().size).isEqualTo(1)
                assertThat(Watcher.getReintroducedTransactions().get(t1.uuid)).isEqualTo(1)
            }
        }

        on ("store blocks") {
            val wallet: Wallet = Wallet();
            val wallet2: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 1, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 1, sleepRange = 0)

            wallet.connect(miner)
            wallet2.connect(miner2)

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }


            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }
            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }

            miner.connect(miner2)
            wallet.connect(miner2)

            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }
            it ("should add new blocks to storage until consensus is reached or receives missing blocks from another node") {
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)).isNotNull()
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)!!.values).hasSize(1)
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)!!.values.first().second).isEqualTo(miner2.getBlockchain().getLastBlockInLongestChain())
            }
        }

        on ("clear blocks from storage") {
            val wallet: Wallet = Wallet();
            val wallet2: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 1, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 1, sleepRange = 0)

            wallet.connect(miner)
            wallet2.connect(miner2)

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }


            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }
            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }

            miner.connect(miner2)
            wallet.connect(miner2)

            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }

            val blockThatShouldBeRemovedFromStorage = miner2.getBlockchain().getLastBlockInLongestChain()

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }

            wallet.connect(wallet2)
            miner.consensus(force=true)
            miner2.consensus(force=true)
            wallet.consensus(force=true)
            wallet2.consensus(force=true)

            it("should clear out storage and record the timestamp") {
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)).isNotNull()
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)!!.values).hasSize(0)
                assertThat(Watcher.getBlocksRemovedFromStorage().get(miner.publicKey)!!.values).hasSize(1)
                val blockTriples = Watcher.getBlocksRemovedFromStorage().get(miner.publicKey)!!.get(blockThatShouldBeRemovedFromStorage.uuid)!!
                assertThat(blockTriples).hasSize(1)
                assertThat(blockTriples.first().first).isLessThanOrEqualTo(blockTriples.first().second)
            }
        }

        on ("add block from storage to blockchain") {
            val wallet: Wallet = Wallet();
            val wallet2: Wallet = Wallet();
            val miner: Miner = Miner(difficulty = 0, sleepRange = 0)
            val miner2: Miner = Miner(difficulty = 0, sleepRange = 0)

            wallet.connect(miner)
            wallet2.connect(miner2)

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }


            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }
            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }

            miner.connect(miner2)
            wallet.connect(miner2)

            wallet2.create_transaction(miner2.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }

            val blocksForVerification: ArrayList<Block> = arrayListOf()

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            blocksForVerification.add(miner.getBlockchain().getLastBlockInLongestChain())

            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            blocksForVerification.add(miner.getBlockchain().getLastBlockInLongestChain())

            wallet.connect(wallet2)
            miner.consensus(force=true)
            miner2.consensus(force=true)
            wallet.consensus(force=true)
            wallet2.consensus(force=true)

            it("should contain all blocks that were added from the storage") {
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)).isNotNull()
                assertThat(Watcher.getBlocksInStorage().get(miner.publicKey)!!.values).hasSize(0)
                assertThat(Watcher.getBlocksAddedFromStorage().get(miner2.publicKey)!!.values).hasSize(2)
                blocksForVerification.forEach{block ->
                    val blockTriples = Watcher.getBlocksAddedFromStorage().get(miner2.publicKey)!!.get(block.uuid)!!
                    assertThat(blockTriples.first).isLessThanOrEqualTo(blockTriples.second)
                }
            }
        }
    }
})
