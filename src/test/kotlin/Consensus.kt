import blockchain.Block
import org.assertj.core.api.Assertions.*
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import blockchain.Miner
import blockchain.Wallet

class Consensus: Spek({
    given("One Miner One Wallet") {
        var wallet: Wallet = Wallet();
        var miner: Miner = Miner(difficulty = 0, sleepRange = 0)
        beforeEachTest {
            wallet = Wallet();
            miner = Miner(difficulty = 0, sleepRange = 0)
            wallet.connect(miner)
        }

        on("init") {
            it ("should have a genesis block") {
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(1)
            }
        }

        on("consensus with no blocks or transactions") {
            wallet.consensus(force = true)
            it ("should not modify the blockchain") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(wallet.getBlockchain().getLastBlockInLongestChain()).isEqualTo(miner.getBlockchain().getLastBlockInLongestChain())
            }
        }

        on("new transaction") {
            wallet.create_transaction(miner.publicKey, "1")
            it ("should not modify the blockchain") {
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(1)
            }
            it ("should add it to the mempool") {
                assertThat(miner.getMempool().getPendingTransactions()).hasSize(1)
                assertThat(wallet.getMempool().getPendingTransactions()).hasSize(1)
            }
        }

        on("multiple new transactions") {
            wallet.create_transaction(miner.publicKey, "1")
            wallet.create_transaction(miner.publicKey, "1")
            it ("should not modify the blockchain") {
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(1)
            }
            it ("should add it to the mempool") {
                assertThat(miner.getMempool().getPendingTransactions()).hasSize(2)
                assertThat(wallet.getMempool().getPendingTransactions()).hasSize(2)
            }
        }

        on("new trans. mine") {
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            it("should not update wallet's bc before consensus") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(2)
            }
        }

        on("new trans. mine. consensus") {
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.consensus(force=true)
            it("should take the longest chain") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(2)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(2)
                assertThat(wallet.getBlockchain().getLastBlockInLongestChain().hash).isEqualTo(miner.getBlockchain().getLastBlockInLongestChain().hash)
            }
            it("should contain all transactions that were in the mempool") {
                assertThat(wallet.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(1)
                assertThat(miner.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(1)
            }
            it("should have an empty mempool") {
                assertThat(miner.getMempool().getPendingTransactions().size).isEqualTo(0)
            }
        }

        on("new trans. new trans. mine. conensus") {
            wallet.create_transaction(miner.publicKey, "1")
            wallet.create_transaction(miner.publicKey, "2")
            miner.mineblocks { i -> i == 1 }
            wallet.consensus(force=true)
            it("should take the longest chain") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(2)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(2)
                assertThat(wallet.getBlockchain().getLastBlockInLongestChain().hash).isEqualTo(miner.getBlockchain().getLastBlockInLongestChain().hash)
            }
            it("should contain all transactions that were in the mempool") {
                assertThat(wallet.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
                assertThat(miner.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
            }
            it("should have an empty mempool") {
                assertThat(miner.getMempool().getPendingTransactions().size).isEqualTo(0)
            }
        }

        on("new trans. mine. new trans. consensus") {
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "2")
            wallet.consensus(force=true)
            it("should take the longest chain") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(2)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(2)
                assertThat(wallet.getBlockchain().getLastBlockInLongestChain().hash).isEqualTo(miner.getBlockchain().getLastBlockInLongestChain().hash)
            }
            it("should contain all transactions that were mined in the mempool") {
                assertThat(wallet.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(1)
                assertThat(miner.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(1)
            }
            it("should have one transaction left in the mempool") {
                assertThat(miner.getMempool().getPendingTransactions().size).isEqualTo(1)
            }
        }

        on("new trans. mine. new trans. mine. consensus") {
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "2")
            miner.mineblocks { i -> i == 1 }
            wallet.consensus(force=true)
            it("should take the longest chain") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(3)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(3)
                assertThat(wallet.getBlockchain().getLastBlockInLongestChain().hash).isEqualTo(miner.getBlockchain().getLastBlockInLongestChain().hash)
            }
            it("should contain all transactions that were mined in the mempool") {
                assertThat(wallet.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
                assertThat(miner.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
            }
            it("should have an empty mempool") {
                assertThat(miner.getMempool().getPendingTransactions().size).isEqualTo(0)
            }
        }

        on("new trans. mine. consensus. new trans. mine. consensus") {
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.consensus(force=true)
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.consensus(force=true)

            it("should take the longest chain") {
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(3)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(3)
                assertThat(wallet.getBlockchain().getLastBlockInLongestChain().hash).isEqualTo(miner.getBlockchain().getLastBlockInLongestChain().hash)
            }
            it("should contain all transactions that were mined in the mempool") {
                assertThat(wallet.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
                assertThat(miner.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
            }
            it("should have an empty mempool") {
                assertThat(miner.getMempool().getPendingTransactions().size).isEqualTo(0)
            }
        }

        on("3rd node joins later on after blocks have been added to the chain") {
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet.consensus(force=true)

            val wallet2 = Wallet(listOf(wallet))
            wallet2.consensus(force=true)

            it("should take the longest chain") {
                assertThat(wallet2.getBlockchain().getLongestHeight()).isEqualTo(3)
                assertThat(wallet2.getBlockchain().getLastBlockInLongestChain().hash).isEqualTo(wallet.getBlockchain().getLastBlockInLongestChain().hash)
            }
            it("should contain all transactions that were mined in the mempool") {
                assertThat(wallet2.getBlockchain().getBlocksInLongestChain().flatMap{ block->(block as Block).data}).hasSize(2)
            }
        }

        on("random consensus between mining and 3 nodes") {
            val miner2 = Miner(0, sleepRange = 0)
            val addBlock = {
                wallet.create_transaction(miner.publicKey, "1")
                miner.mineblocks { i -> i == 1 }
            }

            addBlock()
            addBlock()
            wallet.consensus(force=true)
            miner2.consensus(force=true)
            assertThat(miner2.getBlockchain().getLongestHeight()).isEqualTo(1)
            miner2.connect(wallet)
            miner2.consensus(force=true)
            assertThat(miner2.getBlockchain().getLongestHeight()).isEqualTo(3)

            addBlock()
            addBlock()
            miner2.consensus(force=true)
            assertThat(miner2.getBlockchain().getLongestHeight()).isEqualTo(3)
            miner2.connect(miner)
            miner2.consensus(force=true)
            assertThat(miner2.getBlockchain().getLongestHeight()).isEqualTo(5)

            wallet.create_transaction(miner.publicKey, "1")
            miner2.mineblocks { i -> i == 1 }
            miner.consensus(force=true)
            miner2.consensus(force=true)
            wallet.consensus(force=true)

            it("should have consistent chains between all nodes") {
                assertThat(miner2.getBlockchain().getBlocksInLongestChain())
                        .containsExactlyElementsOf(miner.getBlockchain().getBlocksInLongestChain())
                        .containsExactlyElementsOf(wallet.getBlockchain().getBlocksInLongestChain())
                assertThat(miner2.getBlockchain().getLongestHeight()).isEqualTo(6)
            }
        }

        on("2 groups sync after checkpoints have been reached") {
            val addBlock = {
                wallet.create_transaction(miner.publicKey, "1")
                miner.mineblocks { i -> i == 1 }
            }
            val miner2 = Miner(0, sleepRange = 0)
            val wallet2 = Wallet()
            wallet2.connect(miner2)
            val addBlock2 = {
                wallet2.create_transaction(miner2.publicKey, "2")
                miner2.mineblocks { i -> i == 1 }
            }

            addBlock()
            addBlock()
            addBlock()
            addBlock()
            wallet.consensus(force=true)
            miner.consensus(force=true)

            addBlock2()
            addBlock2()
            addBlock2()
            wallet2.consensus(force=true)
            miner2.consensus(force=true)

            miner2.connect(miner)
            miner2.consensus(force=true)
            wallet2.consensus(force=true)
            wallet.consensus(force=true)
            miner.consensus(force=true)

            it ("should reintroduce transactions that were replaced by the newer chain") {
                assertThat(miner2.getMempool().getPendingTransactions().map { transaction -> transaction.value }).hasSize(3).allMatch({ t -> t == "2" })
                assertThat(miner.getMempool().getPendingTransactions().map { transaction -> transaction.value }).hasSize(3).allMatch { t -> t == "2" }
                assertThat(miner2.getMempool().getPendingTransactions()).containsExactlyElementsOf(miner.getMempool().getPendingTransactions())
            }

            it ("should have consistent chains between all nodes") {
                assertThat(miner2.getBlockchain().getBlocksInLongestChain())
                        .containsExactlyElementsOf(miner.getBlockchain().getBlocksInLongestChain())
                        .containsExactlyElementsOf(wallet.getBlockchain().getBlocksInLongestChain())
                        .containsExactlyElementsOf(wallet2.getBlockchain().getBlocksInLongestChain())
                assertThat(miner2.getBlockchain().getLongestHeight()).isEqualTo(5)
            }
        }

        on("consensus when first connecting") {
            wallet.disconnect(miner)
            val wallet2 = Wallet()
            val wallet3 = Wallet()
            wallet2.connect(miner)
            wallet3.connect(wallet)
            wallet2.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet2.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            wallet2.create_transaction(miner.publicKey, "1")
            miner.mineblocks { i -> i == 1 }
            it("should have different blockchains before connecting to each other") {
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(4)
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(wallet2.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(wallet3.getBlockchain().getLongestHeight()).isEqualTo(1)
            }
            it("should run consensus immediately between only these two nodes after they connect") {
                wallet.connect(miner)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(4)
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(4)
                assertThat(wallet2.getBlockchain().getLongestHeight()).isEqualTo(1)
                assertThat(wallet3.getBlockchain().getLongestHeight()).isEqualTo(1)
            }
            it("should have all equal blockchains after all nodes run consensus") {
                wallet.consensus(true)
                wallet2.consensus(true)
                wallet3.consensus(true)
                miner.consensus(true)
                assertThat(miner.getBlockchain().getLongestHeight()).isEqualTo(4)
                assertThat(wallet.getBlockchain().getLongestHeight()).isEqualTo(4)
                assertThat(wallet2.getBlockchain().getLongestHeight()).isEqualTo(4)
                assertThat(wallet3.getBlockchain().getLongestHeight()).isEqualTo(4)
            }
        }

    }
})